Finished :
<ul>
    <li>Publisher sends messages to the brokers</li>
    <li>Broker receives messages from the publisher</li>
    <li>Broker sends messages to subscriber</li>
    <li>Subscriber receives messages from broker</li>
    <li>implementation of subscription port and connector</li>
</ul>

To Test :
<ul>
    <li>Concurrency in requests</li>
    <li>ConcurrentHashMap</li>
</ul>

TODO  :
<ul>
    <li>Implement the comportment of subscription in Broker</li>
    <li>Adding Log everywhere</li>
    <li>Find a way to filter messages in different categories</li>
    <li>Add the filter in message and in management</li>
    <li>Delegate send part of broker to a scheduled task</li>
    <li>Delegate the responsibility of mapping categories in broker</li>
    <li>Find a way to simplify logging (function for all)</li>
    <li>Add a GUI in the publisher to publish message responding to event</li>
</ul>
