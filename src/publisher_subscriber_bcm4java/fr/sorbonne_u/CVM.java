package publisher_subscriber_bcm4java.fr.sorbonne_u;

import fr.sorbonne_u.components.AbstractComponent;
import fr.sorbonne_u.components.cvm.AbstractCVM;
import publisher_subscriber_bcm4java.fr.sorbonne_u.beans.CVMMyDebug;
import publisher_subscriber_bcm4java.fr.sorbonne_u.components.broker.BrokerComponent;
import publisher_subscriber_bcm4java.fr.sorbonne_u.components.publisher.PublisherComponent;
import publisher_subscriber_bcm4java.fr.sorbonne_u.components.subscriber.SubscriberComponent;
import publisher_subscriber_bcm4java.fr.sorbonne_u.connectors.BrokerSubscriberMessageConnector;
import publisher_subscriber_bcm4java.fr.sorbonne_u.connectors.PublisherBrokerMessageConnector;
import publisher_subscriber_bcm4java.fr.sorbonne_u.connectors.SubscriptionBrokerConnector;

public class CVM extends AbstractCVM {
	public CVM() throws Exception {
		super();
	}

	protected static final String BROKER_URI 								 = "Broker" ;
	protected static final String BROKER_INBOUND_MESSAGE_PORT_URI 			 = "broFromPubPort" ;
	protected static final String BROKER_OUTBOUND_1_MESSAGE_PORT_URI 	     = "broToSub1Port" ;
	protected static final String BROKER_OUTBOUND_2_MESSAGE_PORT_URI 	     = "broToSub2Port" ;
	protected static final String BROKER_INBOUND_SUBSCRIPTION_PORT_URI 		 = "broFromSubscriptionPort" ;

	protected static final String PUBLISHER_URI 							 = "Publisher" ;
	protected static final String PUBLISHER_OUTBOUND_MESSAGE_PORT_URI 		 = "pubToBroMessagePort" ;
	protected static final String PUBLISHER_OUTBOUND_SUBSCRIPTION_PORT_URI 	 = "pubToBroSubscriptionPort" ;

	protected static final String SUBSCRIBER_1_URI 							 = "Subscriber1" ;
	protected static final String SUBSCRIBER_1_INBOUND_MESSAGE_PORT_URI 	 = "sub1FromBroMessagePort" ;
	protected static final String SUBSCRIBER_1_INBOUND_SUBSCRIPTION_PORT_URI = "sub1FromBroSubscriptionPort" ;

	protected static final String SUBSCRIBER_2_URI 							 = "Subscriber2" ;
	protected static final String SUBSCRIBER_2_INBOUND_MESSAGE_PORT_URI 	 = "sub2FromBroMessagePort" ;
	protected static final String SUBSCRIBER_2_INBOUND_SUBSCRIPTION_PORT_URI = "sub2FromBroSubscriptionPort" ;

	protected String uriBrokerURI;
	protected String uriPublisherURI;
	protected String uriSubscriber1URI;
	protected String uriSubscriber2URI;


	@Override
	public void	finalise() throws Exception {
		// Port disconnections can be done here for static architectures
		// otherwise, they can be done in the finalise methods of components.
		this.doPortDisconnection(
				this.uriPublisherURI,
				PUBLISHER_OUTBOUND_MESSAGE_PORT_URI) ;

		super.finalise();
	}
	@Override
	public void	shutdown() throws Exception {
		assert	this.allFinalised() ;
		// any disconnection not done yet can be performed here

		super.shutdown();
	}

	@Override
	public void	deploy() throws Exception {
		assert	!this.deploymentDone() ;

		// ==================================================
		// Configuration phase
		// ==================================================

		// debugging mode configuration; comment and uncomment the line to see
		// the difference
//		AbstractCVM.DEBUG_MODE.add(CVMDebugModes.PUBLIHSING) ;
//		AbstractCVM.DEBUG_MODE.add(CVMDebugModes.CONNECTING) ;
//		AbstractCVM.DEBUG_MODE.add(CVMDebugModes.COMPONENT_DEPLOYMENT) ;
//		AbstractCVM.DEBUG_MODE.add(CVMMyDebug.INTENSE_LOGGING) ;
		AbstractCVM.DEBUG_MODE.add(CVMMyDebug.SIMPLE_LOGGING) ;

		// ==================================================
		// Creation phase
		// ==================================================

//		// create the provider component
		this.uriBrokerURI = AbstractComponent.createComponent(
			BrokerComponent.class.getCanonicalName(),
			new Object[]{
				BROKER_URI,
				new String[]{
					BROKER_INBOUND_MESSAGE_PORT_URI,
				},
				new String[]{
					BROKER_OUTBOUND_1_MESSAGE_PORT_URI,
					BROKER_OUTBOUND_2_MESSAGE_PORT_URI,
				},
				BROKER_INBOUND_SUBSCRIPTION_PORT_URI
			}
		);
		assert	this.isDeployedComponent(this.uriBrokerURI) ;
		// make it trace its operations; comment and uncomment the line to see
		// the difference
		this.toggleTracing(this.uriBrokerURI) ;
		this.toggleLogging(this.uriBrokerURI) ;

		// create the consumer component
		this.uriPublisherURI = AbstractComponent.createComponent(
			PublisherComponent.class.getCanonicalName(),
			new Object[]{
				PUBLISHER_URI,
				PUBLISHER_OUTBOUND_MESSAGE_PORT_URI,
				PUBLISHER_OUTBOUND_SUBSCRIPTION_PORT_URI
			}
		);
		assert	this.isDeployedComponent(this.uriPublisherURI) ;
		// make it trace its operations; comment and uncomment the line to see
		// the difference
		this.toggleTracing(this.uriPublisherURI) ;
		this.toggleLogging(this.uriPublisherURI) ;


		this.uriSubscriber1URI = AbstractComponent.createComponent(
			SubscriberComponent.class.getCanonicalName(),
			new Object[]{
				SUBSCRIBER_1_URI,
				SUBSCRIBER_1_INBOUND_MESSAGE_PORT_URI,
				SUBSCRIBER_1_INBOUND_SUBSCRIPTION_PORT_URI
			}
		);
		assert this.isDeployedComponent(this.uriSubscriber1URI);
		this.toggleLogging(this.uriSubscriber1URI);
		this.toggleTracing(this.uriSubscriber1URI);


		this.uriSubscriber2URI = AbstractComponent.createComponent(
			SubscriberComponent.class.getCanonicalName(),
			new Object[]{
				SUBSCRIBER_2_URI,
				SUBSCRIBER_2_INBOUND_MESSAGE_PORT_URI,
				SUBSCRIBER_2_INBOUND_SUBSCRIPTION_PORT_URI
			}
		);
		assert this.isDeployedComponent(this.uriSubscriber2URI);
		this.toggleLogging(this.uriSubscriber2URI);
		this.toggleTracing(this.uriSubscriber2URI);

		// ==================================================
		// Connection phase
		// ==================================================

		// Message connection Publiser -> Broker
		this.doPortConnection(
			this.uriPublisherURI,
			PUBLISHER_OUTBOUND_MESSAGE_PORT_URI,
			BROKER_INBOUND_MESSAGE_PORT_URI,
			PublisherBrokerMessageConnector.class.getCanonicalName()
		);

		// Message connection Broker -> Subscriber1
		this.doPortConnection(
			this.uriBrokerURI,
			BROKER_OUTBOUND_1_MESSAGE_PORT_URI,
			SUBSCRIBER_1_INBOUND_MESSAGE_PORT_URI,
			BrokerSubscriberMessageConnector.class.getCanonicalName()
		);


		// Message connection Broker -> Subscriber2
		this.doPortConnection(
				this.uriBrokerURI,
				BROKER_OUTBOUND_2_MESSAGE_PORT_URI,
				SUBSCRIBER_2_INBOUND_MESSAGE_PORT_URI,
				BrokerSubscriberMessageConnector.class.getCanonicalName()
		);

		// Subscription connection Publisher -> Broker
		this.doPortConnection(
			this.uriPublisherURI,
			PUBLISHER_OUTBOUND_SUBSCRIPTION_PORT_URI,
			BROKER_INBOUND_SUBSCRIPTION_PORT_URI,
			SubscriptionBrokerConnector.class.getCanonicalName()
		);

		// Subscription connection Subscriber2 -> Broker
		this.doPortConnection(
				this.uriSubscriber2URI,
				SUBSCRIBER_2_INBOUND_SUBSCRIPTION_PORT_URI,
				BROKER_INBOUND_SUBSCRIPTION_PORT_URI,
				SubscriptionBrokerConnector.class.getCanonicalName()
		);


		// Subscription connection Subscriber1 -> Broker
		this.doPortConnection(
			this.uriSubscriber1URI,
			SUBSCRIBER_1_INBOUND_SUBSCRIPTION_PORT_URI,
			BROKER_INBOUND_SUBSCRIPTION_PORT_URI,
			SubscriptionBrokerConnector.class.getCanonicalName()
		);




		// ==================================================
		// Deployment done
		// ==================================================

		super.deploy();
		assert	this.deploymentDone() ;
	}

	
	
	
	public static void	main(String[] args) {
		try {
			// Create an instance of the defined component virtual machine.
			CVM a = new CVM() ;
			// Execute the application.
			a.startStandardLifeCycle(20000L) ;
			// Give some time to see the traces (convenience).
			Thread.sleep(50000L) ;
			// Simplifies the termination (termination has yet to be treated
			// properly in BCM).
			System.exit(0) ;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}
