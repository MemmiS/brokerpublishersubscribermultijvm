package publisher_subscriber_bcm4java.fr.sorbonne_u.components.subscriber;

import fr.sorbonne_u.components.AbstractComponent;
import fr.sorbonne_u.components.annotations.OfferedInterfaces;
import fr.sorbonne_u.components.annotations.RequiredInterfaces;
import fr.sorbonne_u.components.cvm.AbstractCVM;
import fr.sorbonne_u.components.exceptions.ComponentShutdownException;
import fr.sorbonne_u.components.exceptions.ComponentStartException;
import fr.sorbonne_u.components.exceptions.PreconditionException;
import publisher_subscriber_bcm4java.fr.sorbonne_u.beans.CVMMyDebug;
import publisher_subscriber_bcm4java.fr.sorbonne_u.components.subscriber.interfaces.SubscriberMessageI;
import publisher_subscriber_bcm4java.fr.sorbonne_u.components.subscription.OutboundSubscriptionI;
import publisher_subscriber_bcm4java.fr.sorbonne_u.interfaces.IMessage;
import publisher_subscriber_bcm4java.fr.sorbonne_u.interfaces.MessageFilterI;
import publisher_subscriber_bcm4java.fr.sorbonne_u.ports.OutboundPortSubscription;
import publisher_subscriber_bcm4java.fr.sorbonne_u.ports.SubscriberInboundPortMessage;

@OfferedInterfaces(offered = {
        SubscriberMessageI.class,
})
@RequiredInterfaces(required = {
        OutboundSubscriptionI.class,
})
public class SubscriberComponent extends AbstractComponent {
    private static final int SCHEDULABLE_THREAD = 1;
    private static final int RUNNING_THREAD = 1;

    private SubscriberInboundPortMessage messagePort;
    private OutboundPortSubscription subscriptionPort;
    private String myURI;

    // TODO :: change parameters to list
    protected SubscriberComponent(
            String URI,
            String messagePortURI,
            String subscriptionPortURI
    ) throws Exception {
        super(URI, SCHEDULABLE_THREAD, RUNNING_THREAD);
        assert URI != null : new PreconditionException("uri can't be null!");
        assert messagePortURI != null : new PreconditionException("subscriberMessagePortURI can't be null!");
        assert subscriptionPortURI != null : new PreconditionException("subscriberMessagePortURI can't be null!");
        myURI = URI;

        this.messagePort = new SubscriberInboundPortMessage(messagePortURI,this);
        this.messagePort.publishPort();


        this.subscriptionPort = new OutboundPortSubscription(subscriptionPortURI, this);
        this.subscriptionPort.localPublishPort();

        if(subscriptionPortURI.equals("sub2FromBroSubscriptionPort")){
            System.out.println("");
        }

    }


    // ==========================================================================
    //                                  LifeCycle
    // ==========================================================================

    @Override
    public void start() throws ComponentStartException {
        if(AbstractCVM.DEBUG_MODE.contains(CVMMyDebug.SIMPLE_LOGGING)) {
            String msg = "Starting :" + myURI ;
            logMessage(msg);
        }
        super.start();

    }

    @Override
    public void finalise() throws Exception {
        if(AbstractCVM.DEBUG_MODE.contains(CVMMyDebug.SIMPLE_LOGGING)) {
            String msg = "Stopping :" + myURI ;
            logMessage(msg);
        }
        this.messagePort.unpublishPort();
        this.subscriptionPort.unpublishPort();
        super.finalise();
    }

    @Override
    public void shutdown() throws ComponentShutdownException {
        if(AbstractCVM.DEBUG_MODE.contains(CVMMyDebug.SIMPLE_LOGGING)) {
            String msg = "Shut down :" + myURI ;
            logMessage(msg);
        }
        super.shutdown();
    }

    @Override
    public void shutdownNow() throws ComponentShutdownException {
        if(AbstractCVM.DEBUG_MODE.contains(CVMMyDebug.SIMPLE_LOGGING)) {
            String msg = "Shut down NOW :" + myURI ;
            logMessage(msg);
        }
        super.shutdownNow();
    }


    // ==========================================================================
    //                       Messages Services
    // ==========================================================================
    public Void acceptMessageService(IMessage iMessage) throws Exception{
        if(AbstractCVM.DEBUG_MODE.contains(CVMMyDebug.PRINTING_INTENSE)) {
            String msg =  myURI + " "+ this.executionLog.getFileName() + " has received a message from publisher ";
            AbstractCVM.getCVM().logDebug(CVMMyDebug.PRINTING_INTENSE, msg);
        }
        if(AbstractCVM.DEBUG_MODE.contains(CVMMyDebug.SIMPLE_LOGGING)) {
            String msg = myURI + " : has received a message : \n\t" + iMessage.getURI();
            logMessage(msg);
        }
        return null;
    }

    public Void acceptMessagesService(IMessage [] iMessages){
        if(AbstractCVM.DEBUG_MODE.contains(CVMMyDebug.PRINTING_INTENSE)) {
            String msg =  myURI + " "+ this.executionLog.getFileName() + " has received "+ iMessages.length +" messages from publisher ";
            AbstractCVM.getCVM().logDebug(CVMMyDebug.PRINTING_INTENSE, msg);
        }
        if(AbstractCVM.DEBUG_MODE.contains(CVMMyDebug.SIMPLE_LOGGING)) {
            StringBuilder msg = new StringBuilder();
            msg.append(myURI).append(" : has received messages : \n");
            for (IMessage m : iMessages) {
                msg.append("\t").append(m.getURI()).append('\n');
            }
            logMessage(msg.toString());
        }
        return null;
    }



    // ==========================================================================
    //                     Subscription Services
    // ==========================================================================


    public void createTopicService(String topic) throws Exception {
        this.subscriptionPort.createTopic(topic);
    }

    public void createTopicsService(String[] topic) throws Exception {
        this.subscriptionPort.createTopics(topic);
    }

    public void destroyTopicService(String topic) throws Exception {
        this.subscriptionPort.destroyTopic(topic);
    }

    public boolean isTopicService(String topic) throws Exception {
        return this.subscriptionPort.isTopic(topic);
    }

    public String[] getTopicsService() throws Exception {
        return this.subscriptionPort.getTopics();
    }

    public String getPublicationPortURIService() throws Exception {
        return this.subscriptionPort.getPublicationPortURI();
    }

    public void subscribeService(String topic, String inboundPortURI) throws Exception {
        this.subscriptionPort.subscribe(topic, inboundPortURI);
    }

    public void subscribeService(String[] topics, String inboundPortURI) throws Exception {
        this.subscriptionPort.subscribe(topics, inboundPortURI);
    }

    public void subscribeService(
            String topic,
            MessageFilterI filter,
            String inboundPortURI
    ) throws Exception {
        this.subscriptionPort.subscribe(topic, filter, inboundPortURI);
    }

    public void modifyFilterService(
            String topic,
            MessageFilterI newfilter,
            String inboundPortURI
    ) throws Exception {
        this.subscriptionPort.modifyFilter(topic, newfilter, inboundPortURI);
    }

    public void unsubscribeService(String topic, String inboundPortURI) throws Exception {
        this.subscriptionPort.unsubscribe(topic, inboundPortURI);
    }


}
