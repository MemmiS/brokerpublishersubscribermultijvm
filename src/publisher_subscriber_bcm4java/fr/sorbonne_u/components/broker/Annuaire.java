package publisher_subscriber_bcm4java.fr.sorbonne_u.components.broker;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Collectors;

import publisher_subscriber_bcm4java.fr.sorbonne_u.interfaces.IMessage;
import publisher_subscriber_bcm4java.fr.sorbonne_u.interfaces.MessageFilterI;

public class 		Annuaire {

	private 		ReentrantLock 					topicLock;
	private 		Condition 						stillContainsMessages;

	private final 	Object 							mutexTopicUserMessage;
	private final 	Object 							mutexPrefUsers;

	private final 	Object 							mutexMessageTopic;

	private final 	Object 							mutexURIS;

	private 		Map<IMessage, List<String>>     messageToTopics;
	private 		Map<String, List<IMessage>>     topicToMessages;


	private 		Map<String, List<Preference>>   uriToPrefs;
	private 		Map<String, List<String>>       topicToUsers;



	public Annuaire() {
		this.topicToMessages = new ConcurrentHashMap<>();
		this.messageToTopics = new ConcurrentHashMap<>();
		this.uriToPrefs = new ConcurrentHashMap<>();
		this.topicToUsers = new ConcurrentHashMap<>();


		this.topicLock = new ReentrantLock();
		this.stillContainsMessages = topicLock.newCondition();

		this.mutexTopicUserMessage = new Object();
		this.mutexPrefUsers = new Object();
		this.mutexURIS = new Object();
		this.mutexMessageTopic = new Object();
	}

	// ==========================================================================
	//                      		TOPICS
	// ==========================================================================

	public void createTopic(String topic) {
		synchronized (mutexTopicUserMessage) {
			this.topicToUsers.put(topic, new ArrayList<>());
			this.topicToMessages.put(topic, new ArrayList<>());
		}
	}

	public void createTopics(String[] topic) {
		for(String t : topic) {
			createTopic(t);
		}
	}
// HERE
	public void destroyTopic(String topic) {
		// IF TOPIC NOT EMPTY(MESSAGES) WAIT
		this.topicLock.lock();
		if (! isTopic(topic)) {
			this.topicLock.unlock();
			return;
		}
		while (!topicToMessages.get(topic).isEmpty()) {
			// S’il y a toujours des messages non envoyés
			try {
				this.stillContainsMessages.await();
			} catch (InterruptedException ignored) {}
		}
		this.topicToMessages.remove(topic);
		this.topicToUsers.remove(topic);
		this.topicLock.unlock();
	}

	public boolean isTopic(String topic) {
		this.topicLock.lock();
		boolean res = this.topicToUsers.containsKey(topic);
		this.topicLock.unlock();
		return res;
	}

	public String[] getTopics() {
		this.topicLock.lock();
		Object [] res = this.topicToUsers.keySet().toArray();
		this.topicLock.unlock();
		return Arrays.copyOf(res, res.length, String[].class);
	}


	// ==========================================================================
	//                      		URIS
	// ==========================================================================

	public void createUser(String URI) {
		synchronized (mutexURIS) {
			this.uriToPrefs.put(URI, new ArrayList<>());
		}
	}

	public void createUsers(String [] URIS) {
		for(String URI : URIS) {
			createUser(URI);
		}
	}

	public boolean isUser(String URI) {
		synchronized (mutexURIS) {
			return this.uriToPrefs.containsKey(URI);
		}
	}

	public String[] getUsers() {
		synchronized (mutexURIS) {
			Object [] res = this.uriToPrefs.keySet().toArray();
			return Arrays.copyOf(res, res.length, String[].class);
		}
	}

	// ==========================================================================
	//                      		SUBSCRIPTION
	// ==========================================================================

	public void addSubscription(String URI, String topic) {
		addSubscription(URI,topic, null);
	}

	public void addSubscription(String URI, String[] topics) {
		for (String topic : topics) {
			if(! isTopic(topic)) {
				createTopic(topic);
			}
		}
		if (! isUser(URI)) {
			createUser(URI);
		}
		List<Preference> ps = new ArrayList<>();
		for (String topic : topics) {
			Preference p = new Preference(topic);
			ps.add(p);
		}
		synchronized (mutexPrefUsers) {
			List<Preference> preferences = this.uriToPrefs.get(URI);
			preferences.addAll(ps);
			for (String topic : topics) {
				List<String> users = this.topicToUsers.get(topic);
				users.add(URI);
			}
		}
	}

	public void addSubscription(String URI, String topic, MessageFilterI filter) {
		if (! isTopic(topic)) {
			createTopic(topic);
		}
		if (! isUser(URI)) {
			createUser(URI);
		}
		synchronized (mutexPrefUsers) {
			Preference p = new Preference(topic, filter);
			// check if it doesn't exist already
			List<Preference> preferences = this.uriToPrefs.get(URI);
			preferences.add(p);

			List<String> topics = this.topicToUsers.get(topic);
			topics.add(URI);
		}
	}

	public void modifyFilter(String URI, String topic, MessageFilterI newFilter) {
		if (! isTopic(topic)) {
			return ;
		}
		if (! isUser(URI)) {
			return ;
		}
		synchronized (mutexPrefUsers) {
			List<Preference> preferences = this.uriToPrefs.get(URI);
			List<Preference> prefOfTopic = preferences.stream().
					filter((p) -> p.getTopic().equals(topic)).
					collect(Collectors.toList());
			if(prefOfTopic.isEmpty()){
				return;
			}
			assert prefOfTopic.size() == 1;
			Preference pref = prefOfTopic.get(0);
			pref.setFilters(newFilter);
		}
	}

	public void removeSubscription(String URI, String topic) {
		if (! isTopic(topic)) {
			return ;
		}
		if (! isUser(URI)) {
			return ;
		}
		synchronized (mutexPrefUsers) {
			List<Preference> preferences = this.uriToPrefs.get(URI);
			List<Preference> prefOfTopic = preferences.stream().
					filter((p) -> p.getTopic().equals(topic))
					.collect(Collectors.toList());
			if(prefOfTopic.isEmpty()){
				return;
			}
			assert prefOfTopic.size() == 1;
			Preference pref = prefOfTopic.get(0);
			preferences.remove(pref);
		}
	}


	// ==========================================================================
	//                      		MESSAGES
	// ==========================================================================

	public void addMessage(IMessage m, String topic) {
		this.topicLock.lock();
		String uri = m.getTimeStamp().getTimestamper();
		if (!isUser(uri)) {
			createUser(uri);
		}
		if (!isTopic(topic)) {
			createTopic(topic);
		}
		synchronized (this.mutexMessageTopic) {
			List<String> topics = new ArrayList<>();
			topics.add(topic);
			this.messageToTopics.put(m, topics);
			this.topicToMessages.get(topic).add(m);
		}
		this.topicLock.unlock();
	}

	public void addMessage(IMessage m, String[] topics){
		this.topicLock.lock();
		String uri = m.getTimeStamp().getTimestamper();
		if( ! isUser(uri) ) {
			createUser(uri);
		}
		for (String topic : topics){
			if(! isTopic(topic) ) {
				createTopic(topic);
			}
		}
		synchronized (this.mutexMessageTopic){
			List<String> topicsToAdd = Arrays.asList(topics);
			this.messageToTopics.put(m, topicsToAdd);
			for (String topic : topics){
				this.topicToMessages.get(topic).add(m);
			}
//			this.stillContainsMessages.signalAll(); // This is necessary only for remove message
		}
		this.topicLock.unlock();
	}

	public void addMessage(IMessage[] ms, String topic) {
		for(IMessage msg : ms) {
			//switching are tolerated between two messages
			this.addMessage(msg, topic);
		}

	}

	public void addMessage(IMessage[] ms, String[] topics) {
		for(IMessage msg: ms) {
			//switching are tolerated between two messages
			this.addMessage(msg, topics);
		}

	}

	public void destroyMessage(IMessage m){
		this.topicLock.lock();
		synchronized (this.mutexMessageTopic){
			List<String> topics = this.messageToTopics.get(m);
			for (String topic : topics){
				this.topicToMessages.get(topic).remove(m);
			}
			this.messageToTopics.remove(m);

			this.stillContainsMessages.signalAll();
		}
		this.topicLock.unlock();
	}

	// ==========================================================================
	//                      		Getters
	// ==========================================================================

	public List<Preference> getPreferenceOfURI(String URI) {
		return uriToPrefs.get(URI);
	}

	public List<String> getURIsOfTopic(String topic) {
		return topicToUsers.get(topic);
	}

	public List<Preference> getPreferencesOfURI(String URI) {
		return this.uriToPrefs.get(URI);
	}

	public List<IMessage> getMessagesOfTopic(String topic) {
		return this.topicToMessages.get(topic);
	}

	public List<String> getTopicsOfMessage(IMessage msg){
		return this.messageToTopics.get(msg);
	}

	public List<String> getURIsToMessage(IMessage m){
		List<String> uris = new ArrayList<>();
		List<String> topics = this.messageToTopics.get(m);
		for (String topic : topics) {
			uris.addAll(this.topicToUsers.get(topic));
		}
		uris = uris.stream().
				distinct().
				filter((u) -> {
					List<Preference> prefs = getPreferenceOfURI(u).stream().
							filter(p -> topics.contains(p.getTopic())).
							collect(Collectors.toList());
					boolean filter = true;
					for (Preference p : prefs) {
						if (p.getFilters() != null) {
							filter = filter && p.getFilters().filter(m);
						}
					}
					return filter;
				}).
				collect(Collectors.toList());

		return uris;
	}


	// ==========================================================================
	//                      		UTILITIES
	// ==========================================================================

	public boolean containsPreference(List<Preference> prefs, Preference pref) {
		return prefs.
				stream().
				filter( (p) -> p.getTopic().equals(pref.getTopic()) ).
				count() > 1;
	}

}
