package publisher_subscriber_bcm4java.fr.sorbonne_u.components.broker;


import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

import fr.sorbonne_u.components.AbstractComponent;
import fr.sorbonne_u.components.annotations.OfferedInterfaces;
import fr.sorbonne_u.components.annotations.RequiredInterfaces;
import fr.sorbonne_u.components.cvm.AbstractCVM;
import fr.sorbonne_u.components.exceptions.ComponentShutdownException;
import fr.sorbonne_u.components.exceptions.ComponentStartException;
import publisher_subscriber_bcm4java.fr.sorbonne_u.beans.CVMMyDebug;
import publisher_subscriber_bcm4java.fr.sorbonne_u.components.broker.interfaces.BrokerPublisherI;
import publisher_subscriber_bcm4java.fr.sorbonne_u.components.broker.interfaces.BrokerSubscriberI;
import publisher_subscriber_bcm4java.fr.sorbonne_u.components.broker.interfaces.BrokerSubscriptionI;
import publisher_subscriber_bcm4java.fr.sorbonne_u.interfaces.IMessage;
import publisher_subscriber_bcm4java.fr.sorbonne_u.interfaces.MessageFilterI;
import publisher_subscriber_bcm4java.fr.sorbonne_u.ports.BrokerInboundPortMessage;
import publisher_subscriber_bcm4java.fr.sorbonne_u.ports.BrokerOutboundPortMessage;
import publisher_subscriber_bcm4java.fr.sorbonne_u.ports.InboundPortSubscription;

@OfferedInterfaces(offered = {
		BrokerPublisherI.class,
		BrokerSubscriptionI.class
})
@RequiredInterfaces(required = {
		BrokerSubscriberI.class
})
public class BrokerComponent extends AbstractComponent {

	private static final int SCHEDULED_THREAD = 3;
	private static final int RUNNING_THREAD = 3;

	private Annuaire annuaire;

	private List<BrokerInboundPortMessage> incomingMessagePort;
	private List<BrokerOutboundPortMessage> sendingMessagePort;
	private InboundPortSubscription subscriptionPort;

	private String myURI;

	// TODO :: change the inbound/outbound port parameter to arrays
	protected BrokerComponent(
			String 	 URI,
			String[] inboundPortMessageURI,
			String[] outboundPortMessageURI,
			String   inboundPortSubscriptionURI
	) throws Exception {
		super(URI, RUNNING_THREAD, SCHEDULED_THREAD);
		assert URI != null;
		this.myURI = URI;
		
		this.annuaire = new Annuaire();
		
		// initialise le port de Subscription
		this.subscriptionPort = new InboundPortSubscription(inboundPortSubscriptionURI, this);
		this.subscriptionPort.publishPort();

		// Initialise ports from Publishers
		this.incomingMessagePort = new Vector<>();
		for (String impu : inboundPortMessageURI) {
			this.incomingMessagePort.add(new BrokerInboundPortMessage(impu,this));
		}
		for (BrokerInboundPortMessage bim : incomingMessagePort) {
			bim.publishPort();
		}

		// Initialise ports to Subscribers
		this.sendingMessagePort = new Vector<>();
		for (String ompu : outboundPortMessageURI){
			sendingMessagePort.add(new BrokerOutboundPortMessage(ompu, this));
		}
		for (BrokerOutboundPortMessage bom: sendingMessagePort ) {
			bom.localPublishPort();
		}
	}


	// ==========================================================================
	//                     Life Cycle
	// ==========================================================================

	@Override
	public void start() throws ComponentStartException {
		if(AbstractCVM.DEBUG_MODE.contains(CVMMyDebug.SIMPLE_LOGGING)) {
			String msg = "Starting :" + myURI ;
			logMessage(msg);
		}
		super.start();
	}

	@Override
	public void finalise() throws Exception {
		for( BrokerInboundPortMessage bim : incomingMessagePort){
			bim.unpublishPort();
		}
		for( BrokerOutboundPortMessage bom : sendingMessagePort){
			bom.unpublishPort();
		}
		this.subscriptionPort.unpublishPort();
		if(AbstractCVM.DEBUG_MODE.contains(CVMMyDebug.SIMPLE_LOGGING)) {
			String msg = "Stopping :" + myURI ;
			logMessage(msg);
		}
		super.finalise();
	}

	@Override
	public void shutdown() throws ComponentShutdownException {
		if(AbstractCVM.DEBUG_MODE.contains(CVMMyDebug.SIMPLE_LOGGING)) {
			String msg = "Shut down :" + myURI ;
			logMessage(msg);
		}
		super.shutdown();
	}

	@Override
	public void shutdownNow() throws ComponentShutdownException {
		if(AbstractCVM.DEBUG_MODE.contains(CVMMyDebug.SIMPLE_LOGGING)) {
			String msg = "Shut down NOW :" + myURI ;
			logMessage(msg);
		}
		super.shutdownNow();
	}


	// ==========================================================================
	//                     Publisher  Services
	// ==========================================================================

	public Void publishService(IMessage m, String topic) throws Exception {
		if(AbstractCVM.DEBUG_MODE.contains(CVMMyDebug.PRINTING_INTENSE)) {
			String msg =  myURI + " " + this.executionLog.getFileName() + " has received a message from publisher (1 Topic)";
			AbstractCVM.getCVM().logDebug(CVMMyDebug.PRINTING_INTENSE, msg);
		}
		if(AbstractCVM.DEBUG_MODE.contains(CVMMyDebug.SIMPLE_LOGGING)) {
			String msg = myURI + " : has received a message";
			logMessage(msg);
		}

//		Thread.sleep(2000);
		
		annuaire.addMessage(m,topic);
	
		acceptMessageService(m);
		return null;
	}

	public Void publishService(IMessage m, String[] topics) throws Exception{
		if(AbstractCVM.DEBUG_MODE.contains(CVMMyDebug.PRINTING_INTENSE)) {
			String msg =  myURI + " " + this.executionLog.getFileName() + " has received a message from publisher ("+ topics.length + " topics)";
			AbstractCVM.getCVM().logDebug(CVMMyDebug.PRINTING_INTENSE, msg);
		}
		if(AbstractCVM.DEBUG_MODE.contains(CVMMyDebug.SIMPLE_LOGGING)) {
			String msg = myURI + " : has received a message";
			logMessage(msg);
		}
		annuaire.addMessage(m,  topics);
		acceptMessageService(m);
		return null;
	}

	public Void publishService(IMessage[] ms, String topic) throws Exception{
		if(AbstractCVM.DEBUG_MODE.contains(CVMMyDebug.PRINTING_INTENSE)) {
			String msg = myURI + " " + this.executionLog.getFileName() + " has received "+ ms.length +" messages  from publisher (1 topic)";
			AbstractCVM.getCVM().logDebug(CVMMyDebug.PRINTING_INTENSE, msg);
		}
		if(AbstractCVM.DEBUG_MODE.contains(CVMMyDebug.SIMPLE_LOGGING)) {
			String msg = myURI + " : has received a message";
			logMessage(msg);
		}
		
		annuaire.addMessage(ms, topic);
		
	
		acceptMessagesService(ms);
		return null;
	}

	public Void publishService(IMessage[] ms, String[] topics) throws Exception {
		if(AbstractCVM.DEBUG_MODE.contains(CVMMyDebug.PRINTING_INTENSE)) {
			String msg =  myURI + " "+ this.executionLog.getFileName() + " has received "+ ms.length + " messages from publisher ("+ topics.length + " topics)";
			AbstractCVM.getCVM().logDebug(CVMMyDebug.PRINTING_INTENSE, msg);
		}
		if(AbstractCVM.DEBUG_MODE.contains(CVMMyDebug.SIMPLE_LOGGING)) {
			String msg = myURI + " : has received a message";
			logMessage(msg);
		}
		
		annuaire.addMessage(ms,  topics);
		

		acceptMessagesService(ms);
		return null;
	}


	// ==========================================================================
	//                      SubscriberServices  Services
	// ==========================================================================

	public void acceptMessageService(IMessage iMessage) throws Exception {
		if(AbstractCVM.DEBUG_MODE.contains(CVMMyDebug.PRINTING_INTENSE)) {
			String msg =  myURI + " "+ this.executionLog.getFileName() + " has sent a message from publisher";
			AbstractCVM.getCVM().logDebug(CVMMyDebug.PRINTING_INTENSE, msg);
		}
		if(AbstractCVM.DEBUG_MODE.contains(CVMMyDebug.SIMPLE_LOGGING)) {
			String msg = myURI + " : has sent a message";
			logMessage(msg);
		}
		for (BrokerOutboundPortMessage bom : sendingMessagePort) {
			bom.acceptMessage(iMessage);
		}
	}

	public void acceptMessagesService(IMessage [] iMessages) throws Exception{
		if(AbstractCVM.DEBUG_MODE.contains(CVMMyDebug.PRINTING_INTENSE)) {
			String msg =  myURI + " "+ this.executionLog.getFileName() + " has sent messages from publisher";
			AbstractCVM.getCVM().logDebug(CVMMyDebug.PRINTING_INTENSE, msg);
		}
		if(AbstractCVM.DEBUG_MODE.contains(CVMMyDebug.SIMPLE_LOGGING)) {
			String msg = myURI + " : has sent messages";
			logMessage(msg);
		}
		for (BrokerOutboundPortMessage bom : sendingMessagePort) {
			bom.acceptMessages(iMessages);
		}
	}


	// ==========================================================================
	//                      Subscription  Services
	// ==========================================================================


	public Void createTopic(String topic) throws Exception {
		annuaire.createTopic( topic);
		return null;
	}

	public Void createTopics(String[] topic) throws Exception {
		annuaire.createTopics(topic);
		return null;
	}

	public Void destroyTopic(String topic) throws Exception {
		annuaire.destroyTopic(topic);
		return null;
	}

	public boolean isTopic(String topic) throws Exception {
		return annuaire.isTopic(topic);
	}

	public String[] getTopics() throws Exception {
		return annuaire.getTopics();
	}
	
	// TODO :: Voir ce que fait cette méthode
	public String getPublicationPortURI() throws Exception {
		return null;
	}

	public Void subscribe(String topic, String inboundPortURI) throws Exception {
		annuaire.addSubscription(inboundPortURI, topic);
		return null;
	}

	public Void subscribe(String[] topics, String inboundPortURI) throws Exception {
		annuaire.addSubscription(inboundPortURI, topics);
		return null;
	}

	public Void subscribe(String topic, MessageFilterI filter, String inboundPortURI) throws Exception {
		annuaire.addSubscription(inboundPortURI, topic, filter);
		return null;
	}

	public Void modifyFilter(String topic, MessageFilterI newfilter, String inboundPortURI) throws Exception {
		annuaire.modifyFilter(inboundPortURI, topic, newfilter);
		return null;
	}

	public Void unsubscribe(String topic, String inboundPortURI) throws Exception {
		annuaire.removeSubscription(inboundPortURI, topic);
		return null;
	}
	
}
