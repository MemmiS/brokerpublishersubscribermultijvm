package publisher_subscriber_bcm4java.fr.sorbonne_u.components.publisher;

import java.util.concurrent.TimeUnit;

import fr.sorbonne_u.components.AbstractComponent;
import fr.sorbonne_u.components.annotations.RequiredInterfaces;
import fr.sorbonne_u.components.cvm.AbstractCVM;
import fr.sorbonne_u.components.exceptions.ComponentShutdownException;
import fr.sorbonne_u.components.exceptions.ComponentStartException;
import fr.sorbonne_u.components.exceptions.PreconditionException;
import publisher_subscriber_bcm4java.fr.sorbonne_u.beans.CVMMyDebug;
import publisher_subscriber_bcm4java.fr.sorbonne_u.beans.MessageFactory;
import publisher_subscriber_bcm4java.fr.sorbonne_u.components.subscription.OutboundSubscriptionI;
import publisher_subscriber_bcm4java.fr.sorbonne_u.interfaces.IMessage;
import publisher_subscriber_bcm4java.fr.sorbonne_u.interfaces.IMessageFactory;
import publisher_subscriber_bcm4java.fr.sorbonne_u.components.publisher.interfaces.PublisherMessageI;
import publisher_subscriber_bcm4java.fr.sorbonne_u.interfaces.MessageFilterI;
import publisher_subscriber_bcm4java.fr.sorbonne_u.ports.PublisherOutboundPortMessage;
import publisher_subscriber_bcm4java.fr.sorbonne_u.ports.OutboundPortSubscription;

@RequiredInterfaces(required = {
		PublisherMessageI.class,
		OutboundSubscriptionI.class,
})
public class PublisherComponent extends AbstractComponent {
	private static final int SCHEDULABLE_THREAD = 1;
	private static final int RUNNING_THREAD = 1;

	private final IMessageFactory factory= new MessageFactory();

	protected final String myURI;

	protected PublisherOutboundPortMessage outboundPortMessage;
	protected OutboundPortSubscription outboundPortSubscription;

	// TODO :: change parameters to arrays
	protected PublisherComponent(
			String URI,
			String outboundPortMessage,
			String outboundPortSubscription
	) throws Exception{

		super(URI, RUNNING_THREAD,SCHEDULABLE_THREAD);
		assert URI != null : new PreconditionException("uri can't be null!");
		assert outboundPortMessage != null : new PreconditionException("outboundPortMessage can't be null!");
		assert outboundPortSubscription != null : new PreconditionException("outboundPortSubscription can't be null!");
		this.myURI = URI;


		this.outboundPortMessage =
				new PublisherOutboundPortMessage(outboundPortMessage,this);
		this.outboundPortMessage.localPublishPort();

		this.outboundPortSubscription =
				new OutboundPortSubscription(outboundPortSubscription, this);
		this.outboundPortSubscription.localPublishPort();
	}


	private int iteration = 0;
	private void action() {
		++iteration;
		if(iteration > 10 ) {
			return;
		}
		IMessage m = factory.newMessage(""+iteration, this.myURI );
		try {
			publishService(m, "Java infer");
		} catch (Exception e) {
			e.printStackTrace();
		}
		this.scheduleTask(
				new AbstractComponent.AbstractTask() {
					@Override
					public void run() {
						action();
					}
				},
				1000, TimeUnit.MILLISECONDS
		);
	}

	// ==========================================================================
	//                                  LifeCycle
	// ==========================================================================
	
	@Override
	public void start() throws ComponentStartException {
		if(AbstractCVM.DEBUG_MODE.contains(CVMMyDebug.SIMPLE_LOGGING)) {
			String msg = "Starting :" + myURI ;
			logMessage(msg);
		}
		super.start();
		this.scheduleTask(
				new AbstractComponent.AbstractTask() {
					@Override
					public void run() {
						action();
					}
				},
				1000, TimeUnit.MILLISECONDS
		);
	}

	@Override
	public void finalise() throws Exception {
		outboundPortMessage.unpublishPort();
		outboundPortSubscription.unpublishPort();
		if(AbstractCVM.DEBUG_MODE.contains(CVMMyDebug.SIMPLE_LOGGING)) {
			String msg = "Stopping :" + myURI ;
			logMessage(msg);
		}
//		this.printExecutionLogOnFile("provider");
		super.finalise();
	}

	@Override
	public void shutdown() throws ComponentShutdownException {
		if(AbstractCVM.DEBUG_MODE.contains(CVMMyDebug.SIMPLE_LOGGING)) {
			String msg = "Shut down :" + myURI ;
			logMessage(msg);
		}
		super.shutdown();
	}

	@Override
	public void shutdownNow() throws ComponentShutdownException {
		try {
			outboundPortMessage.unpublishPort();
		}
		catch(Exception e) {
			throw new ComponentShutdownException(e);
		}
		if(AbstractCVM.DEBUG_MODE.contains(CVMMyDebug.SIMPLE_LOGGING)) {
			String msg = "Shut down NOW :" + myURI ;
			logMessage(msg);
		}
		super.shutdownNow();
	}


	// ==========================================================================
	//                       Message Services
	// ==========================================================================

	public Void publishService(IMessage m, String topic) throws Exception {
		if(AbstractCVM.DEBUG_MODE.contains(CVMMyDebug.PRINTING_INTENSE)) {
			String msg = myURI + " : has send a message";
			AbstractCVM.getCVM().logDebug(CVMMyDebug.PRINTING_INTENSE, msg);
		}
		if(AbstractCVM.DEBUG_MODE.contains(CVMMyDebug.SIMPLE_LOGGING)) {
			String msg = myURI + " : has send a message";
			logMessage(msg);
		}
		outboundPortMessage.publish(m, topic);
		return null;
	}

	public Void publishService(IMessage m, String[] topics) throws Exception {
		if(AbstractCVM.DEBUG_MODE.contains(CVMMyDebug.PRINTING_INTENSE)) {
			String msg = "Publisher " + this.myURI + " has Send a message To Broker ("+ topics.length + " topics)";
			AbstractCVM.getCVM().logDebug(CVMMyDebug.PRINTING_INTENSE, msg);
		}
		if(AbstractCVM.DEBUG_MODE.contains(CVMMyDebug.SIMPLE_LOGGING)) {
			String msg = myURI + " : has send a message";
			logMessage(msg);
		}
		outboundPortMessage.publish(m, topics);
		return null;
	}

	public Void publishService(IMessage[] ms, String topic) throws Exception {
		if(AbstractCVM.DEBUG_MODE.contains(CVMMyDebug.PRINTING_INTENSE)) {
			String msg = "Publisher " + this.myURI + " has Send "+ ms.length +" messages  To Broker (1 topic)";
			AbstractCVM.getCVM().logDebug(CVMMyDebug.PRINTING_INTENSE, msg);
		}
		if(AbstractCVM.DEBUG_MODE.contains(CVMMyDebug.SIMPLE_LOGGING)) {
			String msg = myURI + " : has send a message";
			logMessage(msg);
		}
		outboundPortMessage.publish(ms, topic);
		return null;
	}

	public Void publishService(IMessage[] ms, String[] topics) throws Exception {
		if(AbstractCVM.DEBUG_MODE.contains(CVMMyDebug.PRINTING_INTENSE)) {
			String msg = "Publisher " + this.myURI + " has Send "+ ms.length + " messages To Broker ("+ topics.length + " topics)";
			AbstractCVM.getCVM().logDebug(CVMMyDebug.PRINTING_INTENSE, msg);
		}
		if(AbstractCVM.DEBUG_MODE.contains(CVMMyDebug.SIMPLE_LOGGING)) {
			String msg = myURI + " : has send a message";
			logMessage(msg);
		}
		outboundPortMessage.publish(ms, topics);
		return null;
	}


	// ==========================================================================
	//                     Subscription Services
	// ==========================================================================


	public void createTopicService(String topic) throws Exception {
		this.outboundPortSubscription.createTopic(topic);
	}

	public void createTopicsService(String[] topic) throws Exception {
		this.outboundPortSubscription.createTopics(topic);
	}

	public void destroyTopicService(String topic) throws Exception {
		this.outboundPortSubscription.destroyTopic(topic);
	}

	public boolean isTopicService(String topic) throws Exception {
		return this.outboundPortSubscription.isTopic(topic);
	}

	public String[] getTopicsService() throws Exception {
		return this.outboundPortSubscription.getTopics();
	}

	public String getPublicationPortURIService() throws Exception {
		return this.outboundPortSubscription.getPublicationPortURI();
	}

	public void subscribeService(String topic, String inboundPortURI) throws Exception {
		this.outboundPortSubscription.subscribe(topic, inboundPortURI);
	}

	public void subscribeService(String[] topics, String inboundPortURI) throws Exception {
		this.outboundPortSubscription.subscribe(topics, inboundPortURI);
	}

	public void subscribeService(String topic, MessageFilterI filter, String inboundPortURI) throws Exception {
		this.outboundPortSubscription.subscribe(topic, filter, inboundPortURI);
	}

	public void modifyFilterService(String topic, MessageFilterI newfilter, String inboundPortURI) throws Exception {
		this.outboundPortSubscription.modifyFilter(topic, newfilter, inboundPortURI);
	}

	public void unsubscribeService(String topic, String inboundPortURI) throws Exception {
		this.outboundPortSubscription.unsubscribe(topic, inboundPortURI);
	}
}
