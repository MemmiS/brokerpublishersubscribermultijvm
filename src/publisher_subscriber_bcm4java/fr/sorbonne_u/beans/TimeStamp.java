package publisher_subscriber_bcm4java.fr.sorbonne_u.beans;

import publisher_subscriber_bcm4java.fr.sorbonne_u.interfaces.IMessage;

public class TimeStamp {
	protected long time;
	protected String timestamper;

	//Constructeur était en protected 
	protected TimeStamp(long time, String timestamper) {
		this.time = time;
		this.timestamper = timestamper;
	}
	
	public boolean isInitialised() {
		return timestamper != null;
	}

	// this should not exist

	public long getTime() {
		return time;
	}

	public void setTime(long time) {
		this.time = time;
	}

	public String getTimestamper() {
		return timestamper;
	}

	public void setTimestamper(String timestamper) {
		this.timestamper = timestamper;
	}
}
