package publisher_subscriber_bcm4java.fr.sorbonne_u.beans;

import java.io.Serializable;

import publisher_subscriber_bcm4java.fr.sorbonne_u.interfaces.IMessage;

public class Message implements IMessage {

	private String URI;
	private Serializable payload;
	private Properties properties;
	private TimeStamp timeStamp;

	//Constructeur était protected
	protected Message(Serializable content, String URI, TimeStamp timeStamp) {
		this.payload = content;
		this.properties = new Properties();
		this.timeStamp = timeStamp;
		this.URI = URI;
	}

	@Override
	public String getURI() {
		return URI;
	}

	@Override
	public TimeStamp getTimeStamp() {
		return timeStamp;
	}

	@Override
	public Properties getProperties() {
		return properties;
	}

	@Override
	public Serializable getPayload() {
		return payload;
	}

}
