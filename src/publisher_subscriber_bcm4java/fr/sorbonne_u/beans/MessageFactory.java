package publisher_subscriber_bcm4java.fr.sorbonne_u.beans;

import publisher_subscriber_bcm4java.fr.sorbonne_u.interfaces.IMessage;
import publisher_subscriber_bcm4java.fr.sorbonne_u.interfaces.IMessageFactory;

import java.io.Serializable;

public class MessageFactory implements IMessageFactory {

	private static int compteur = 0;
	@Override
	public IMessage newMessage(Serializable msg, String providersURI) {
		String uriMessage = generateUIDForMessage();
		long time = System.currentTimeMillis();
		TimeStamp stamp = getTimeStamp(time, providersURI);
		return new Message(msg,uriMessage,stamp);
	}

	@Override
	public String generateUIDForMessage() {
		return "Msg " + (++compteur);
	}

	@Override
	public TimeStamp getTimeStamp(long time, String providersURI) {
		return new TimeStamp(time, providersURI);
	}
}
