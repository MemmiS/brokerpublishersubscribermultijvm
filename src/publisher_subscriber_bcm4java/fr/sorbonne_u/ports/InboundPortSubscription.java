package publisher_subscriber_bcm4java.fr.sorbonne_u.ports;

import fr.sorbonne_u.components.ComponentI;
import fr.sorbonne_u.components.ports.AbstractInboundPort;
import publisher_subscriber_bcm4java.fr.sorbonne_u.components.broker.BrokerComponent;
import publisher_subscriber_bcm4java.fr.sorbonne_u.components.broker.interfaces.BrokerSubscriptionI;
import publisher_subscriber_bcm4java.fr.sorbonne_u.interfaces.MessageFilterI;

public class InboundPortSubscription
    extends AbstractInboundPort
    implements BrokerSubscriptionI {

    public InboundPortSubscription(String uri, ComponentI owner) throws Exception {
        super(uri, BrokerSubscriptionI.class, owner);
    }



    @Override
    public void createTopic(String topic) throws Exception{
        this.getOwner().handleRequestSync(
                owner -> {
                    return ((BrokerComponent)owner).createTopic(topic);
                }
        );
    }

    @Override
    public void createTopics(String[] topic) throws Exception {
        this.getOwner().handleRequestSync(
                owner -> {
                    return ((BrokerComponent)owner).createTopics(topic);
                }
        );
    }

    @Override
    public void destroyTopic(String topic) throws Exception {
        this.getOwner().handleRequestSync(
                owner -> {
                    return ((BrokerComponent)owner).destroyTopic(topic);
                }
        );
    }

    @Override
    public boolean isTopic(String topic) throws Exception {
        return this.getOwner().handleRequestSync(
                owner -> ((BrokerComponent)owner).isTopic(topic)
        );
    }

    @Override
    public String[] getTopics() throws Exception {
        return this.getOwner().handleRequestSync(
                owner -> ((BrokerComponent)owner).getTopics()
        );

//        return new String[0];
    }

    @Override
    public String getPublicationPortURI() throws Exception {
        return this.getOwner().handleRequestSync(
                owner -> ((BrokerComponent)owner).getPublicationPortURI()
        );
    }

    @Override
    public void subscribe(String topic, String inboundPortURI) throws Exception {
        this.getOwner().handleRequestSync(
                owner -> ((BrokerComponent)owner).subscribe(topic, inboundPortURI)
        );
    }

    @Override
    public void subscribe(String[] topics, String inboundPortURI) throws Exception {
        this.getOwner().handleRequestSync(
                owner -> {
                    return ((BrokerComponent)owner).subscribe(topics, inboundPortURI);
                }
        );
    }

    @Override
    public void subscribe(String topic, MessageFilterI filter, String inboundPortURI) throws Exception {
        this.getOwner().handleRequestSync(
                owner -> {
                    return ((BrokerComponent)owner).subscribe(topic, filter, inboundPortURI);
                }
        );
    }

    @Override
    public void modifyFilter(String topic, MessageFilterI newfilter, String inboundPortURI) throws Exception {
        this.getOwner().handleRequestSync(
                owner -> {
                    return ((BrokerComponent)owner).modifyFilter(topic, newfilter, inboundPortURI);
                }
        );
    }

    @Override
    public void unsubscribe(String topic, String inboundPortURI) throws Exception {
        this.getOwner().handleRequestSync(
                owner -> {
                    return ((BrokerComponent)owner).unsubscribe(topic, inboundPortURI);
                }
        );
    }
}
